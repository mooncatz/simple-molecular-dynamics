# Solving Newtons equations of motion in two dimensions for a system of N particles using velocity Verlet integration.
# https://en.wikipedia.org/wiki/Verlet_integration


from datetime import datetime
from matplotlib.animation import FuncAnimation
from multiprocessing import Process, Queue
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import time


class System2D:

    def __init__(
        self,
        n_particles: int = 2,
        max_coord: float = 0.5,
        min_coord: float = -0.5,
        max_vel: float = 1,
        min_vel: float = -1,
        min_mass: int = 1,
        max_mass: int = 10,
        G: float = 6.674,
        force_multiplier: float = 1.0,
        limit_range_of_force=False,
        range_of_force=np.inf,
        random_state = None, 
        min_charge: float = -1,
        max_charge: float = 1,
        exclude_zero_charge: bool = False,
        e: float = 8.854, 
        ):
        """Simulates particles subject to coulomb interaction.

        Args:
            n_particles -- number of particles
            max_coord -- maximum initial x and y value for coordinates.     Unit : [nm]
            min_coord -- minimum initial x and y value for coordinates.     Unit : [nm]
            max_vel -- maximum initial x and y value for velovities         Unit : [m/s]
            min_vel -- minimum initial x and y value for velocitis          Unit : [m/s]
            max_mass -- maximum mass of a particle                          Unit : [u] = [1.6605402 * 10^-27 kg]
            min_mass -- minimum mass for a particle                         Unit : [u] = [1.6605402 * 10^-27 kg]
            G -- gravitational constant                                     Unit : 10−11 m^3 / (kg * s^2)
            force_multiplier -- multiplies the force by this factor
            limit_range_of_force -- whether or not to limit the distance over which the force is applied.
            range_of_force -- the range to which the force is limited (if limitation is applied).
            random_state -- a numpy.random.RandomState instance
        """
        # Generate a random state so that local instances started by multiprocessing.Process all get different random states. (this was an issue earlier)
        if random_state is None:
            random_state = np.random.RandomState(random.SystemRandom().randint(1, 2**32))  # initialize with a random integer
        self.n_particles = n_particles
        self.max_coord = max_coord
        self.min_coord = min_coord
        self.max_vel = max_vel
        self.min_vel = min_vel
        self.pos = self.get_initial_positions(random_state, min_coord, max_coord, n_particles)#random_state.uniform(min_coord, max_coord, (n_particles, 2))    # random initial positions
        self.vel = random_state.uniform(min_vel, max_vel, (n_particles, 2))         # random initial velocities
        self.acc = np.zeros((n_particles, 2))                                       # zero initial acceleration, just a placeholder, this initial zero acceleration is never used in calculations
        self.max_mass = max_mass
        self.min_mass = min_mass
        self.m = random_state.randint(min_mass, max_mass+1, (n_particles))          # random masses
        # GRAVITY SPECIFICS
        self.G = G
        # COULOMB SPECIFICS
        self.max_charge = max_charge
        self.min_charge = min_charge
        if exclude_zero_charge:
            integers = [i for i in range(min_charge, max_charge + 1) if i != 0]
            self.q = np.random.choice(integers, size=(n_particles))     # random charges excluding 0
        else:
            self.q = np.random.randint(min_charge, max_charge + 1, (n_particles))            # random charges
        self.e = e
        # GENERAL STUFF
        self.force_multiplier = force_multiplier
        self.limit_range_of_force = limit_range_of_force
        self.range_of_force = range_of_force
        return

    def get_initial_positions(self, random_state, min_coord, max_coord, n_particles):
        """Samples initial positions from a grid. Use this to avoid particles coming too close to eachother."""

        # Build all coimbinations of x and y coordinates in a grid
        sqrt_n = int(np.sqrt(n_particles))
        sqrt_n_error = n_particles - int(np.sqrt(n_particles))**2
        sqrt_n += sqrt_n_error
        x = np.linspace(min_coord, max_coord, sqrt_n)      # shape --> (sqrt_n)
        y = np.linspace(min_coord, max_coord, sqrt_n)      # shape --> (sqrt_n)
        xv, yv = np.meshgrid(x, y)                              # shape --> (sqrt_n, sqrt_n), (sqrt_n, sqrt_n)
        xv = np.expand_dims(xv, axis=-1)                        # shape --> (sqrt_n, sqrt_n, 1)
        yv = np.expand_dims(yv, axis=-1)                        # shape --> (sqrt_n, sqrt_n, 1)
        grid = np.concatenate([xv, yv], axis=-1)                # shape --> (sqrt_n, sqrt_n, 2)
        pos = np.reshape(grid, (sqrt_n * sqrt_n, 2))  # shape --> (sqrt_n*sqrt_n, 2)
        # Sample random coordinates from the grid and use these as initial positions
        idx = [i for i in range(sqrt_n**2)]
        idx = random_state.choice(idx, size=n_particles, replace=False)
        pos = pos[idx]
        return pos

    def acceleration(self, pos, character, divzero=1e-9):


        if character == 'gravity':
            return self.gravitational_acceleration(pos, divzero)
        if character == 'coulomb':
            return self.coulomb_acceleration(pos, divzero)
        if character == 'both':
            acc_g = self.gravitational_acceleration(pos, divzero)
            acc_c = self.coulomb_acceleration(pos, divzero)
            return acc_g + acc_c

    def gravitational_acceleration(self, pos, divzero=1e-9):
        """Calculates the coulomb force on each particle due to all neighbours.
        
        Args:
            pos -- coordinates of all particles, array of shape (n_particles, 2)
            divzero -- is added to the divisor, when calculating the force, so that division by zero is avoided
        Returns:
            acc -- the acceleration of each particle, array of shape (n_particles) 
        """
        x = pos[:, 0]                                                           # shape --> (N)
        y = pos[:, 1]                                                           # shape --> (N)
        
        x_tmp = np.repeat(x[np.newaxis, ...], repeats=self.n_particles, axis=0) # shape --> (N, N)
        y_tmp = np.repeat(y[np.newaxis, ...], repeats=self.n_particles, axis=0) # shape --> (N, N)
        dx = x_tmp - x_tmp.T                                                    # shape --> (N, N)  # signed distances between every pair of particles, i.e. all possible subtraction combinations as a square matrix  # element mn is the distance from m to n, element nm is the distance from n to m (which has the opposite sign)
        dy = y_tmp - y_tmp.T                                                    # shape --> (N, N)  # signed distances between every pair of particles, i.e. all possible subtraction combinations as a square matrix  # element mn is the distance from m to n, element nm is the distance from n to m (which has the opposite sign)        
        r = np.sqrt(dx**2 + dy**2)                                              # shape --> (N, N)  # euclidean distance  # element mn is the euclidean distance from particle m to n
        if self.limit_range_of_force:
            r[r >= self.range_of_force] = np.inf                                # The force will be zero for these particles
        mm = np.outer(self.m, self.m)                                           # shape --> (N, N)  # all combinations of m1 and m2
        G = self.G
        Fx = G * mm * dx / (r**3 + divzero)                                     # shape --> (N, N)  # force acting on every particle  # element mn is the force on paricle m due to n  # division by zero on diagonal is intentional since r is zero on the diagonal
        Fy = G * mm * dy / (r**3 + divzero)                                     # shape --> (N, N)  # force acting on every particle  # element mn is the force on paricle m due to n  # division by zero on diagonal is intentional since r is zero on the diagonal
        # Row m in Fx is all the forces particle m experience.
        # The net force on particle m is just the sum of all contributions from particles n
        Fx = np.sum(Fx, axis=-1) * self.force_multiplier                        # shape --> (N)
        Fy = np.sum(Fy, axis=-1) * self.force_multiplier                        # shape --> (N)
        m = self.m                                                              # shape --> (N)
        accx = Fx / m                                                           # shape --> (N)     # acceleration of each particle  
        accy = Fy / m                                                           # shape --> (N)     # acceleration of each particle 
        accx = np.expand_dims(accx, axis=-1)                                    # shape --> (N, 1)
        accy = np.expand_dims(accy, axis=-1)                                    # shape --> (N, 1)
        acc = np.concatenate([accx, accy], axis=-1)                             # shape --> (N, 2)
        return acc

    def coulomb_acceleration(self, pos, divzero=1e-9):
        """Calculates the coulomb force on each particle due to all neighbours.
        
        Args:
            pos -- coordinates of all particles, array of shape (n_particles, 2)
            q -- charge of each particle, array of shape (n_particles)
            m -- mass of each particle, array of shape (n_particles)
            divzero -- is added to the divisor, when calculating the force, so that division by zero is avoided
        Returns:
            acc -- the acceleration of each particle, array of shape (n_particles) 
        """
        x = pos[:, 0]                                                           # shape --> (N)
        y = pos[:, 1]                                                           # shape --> (N)
        
        x_tmp = np.repeat(x[np.newaxis, ...], repeats=self.n_particles, axis=0) # shape --> (N, N)
        y_tmp = np.repeat(y[np.newaxis, ...], repeats=self.n_particles, axis=0) # shape --> (N, N)
        dx = x_tmp.T - x_tmp                                                    # shape --> (N, N)  # signed distances between every pair of particles, i.e. all possible subtraction combinations as a square matrix  # element mn is the distance from m to n, element nm is the distance from n to m (which has the opposite sign)
        dy = y_tmp.T - y_tmp                                                    # shape --> (N, N)  # signed distances between every pair of particles, i.e. all possible subtraction combinations as a square matrix  # element mn is the distance from m to n, element nm is the distance from n to m (which has the opposite sign)        
        r = np.sqrt(dx**2 + dy**2)                                              # shape --> (N, N)  # euclidean distance  # element mn is the euclidean distance from particle m to n
        if self.limit_range_of_force:
            r[r >= self.range_of_force] = np.inf                                # The force will be zero for these particles
        qq = np.outer(self.q, self.q)                                                     # shape --> (N, N)  # all combinations of q1 and q2
        k = 1 #1 / (4 * np.pi * self.e) 
        Fx = k * qq * dx / (r**3 + divzero)                                   # shape --> (N, N)  # force acting on every particle  # element mn is the force on paricle m due to n  # division by zero on diagonal is intentional since r is zero on the diagonal
        Fy = k * qq * dy / (r**3 + divzero)                                   # shape --> (N, N)  # force acting on every particle  # element mn is the force on paricle m due to n  # division by zero on diagonal is intentional since r is zero on the diagonal
        # Row m in Fx is all the forces particle m experience.
        # The net force on particle m is just the sum of all contributions from particles n
        Fx = np.sum(Fx, axis=-1) * self.force_multiplier                        # shape --> (N)
        Fy = np.sum(Fy, axis=-1) * self.force_multiplier                        # shape --> (N)
        m = self.m                                                              # shape --> (N)
        accx = Fx / m                                                           # shape --> (N)     # acceleration of each particle  
        accy = Fy / m                                                           # shape --> (N)     # acceleration of each particle 
        accx = np.expand_dims(accx, axis=-1)                                    # shape --> (N, 1)
        accy = np.expand_dims(accy, axis=-1)                                    # shape --> (N, 1)
        acc = np.concatenate([accx, accy], axis=-1)                             # shape --> (N, 2)
        return acc
    
    def update(self, dt: float, character: str):
        """Calculates new position, velocity and acceleration.
        
        Args:
            dt -- size of the time step in arbitrary units
            character -- the type of force to apply, `gravity` or `coulomb`
        Returns:
            None
        Sets:
            self.pos -- updates with new positions
            self.vel -- updates with new velocities
            self.acc -- updates with new accelerations
        """
        # At time t
        pos = self.pos
        vel = self.vel
        acc = self.acceleration(pos, character=character)

        # At time t + dt
        new_pos = pos + vel * dt + 1/2 * acc * dt**2
        new_acc = self.acceleration(new_pos, character=character)
        new_vel = vel + 1/2 * (acc + new_acc) * dt

        self.pos = new_pos
        self.vel = new_vel
        self.acc = new_acc
        return 

    def run(self, dt: float, n_iters: int, character: str, return_every_nth_step: int = 1):
        """Runs a simulation for n_iters iterations using time step size dt.
        
        Args:
            dt -- size of the time step in arbitrary units
            n_iters -- number of iterations
            character -- the type of force to apply, `gravity` or `coulomb`
            return_every_nth_step -- return every this many time steps, instead of every time step.
        Returns:
            array of shape (n_iters, n_particles, 2)
        """
        # Record simulated trajectories
        x = [self.pos[:, 0:1].T]                    # shape --> [(1, n_particles)]
        y = [self.pos[:, 1:2].T]                    # shape --> [(1, n_particles)]
        for i in range(n_iters-1):
            if i % 1000 == 0:
                print("\rIteration " + str(i) + "/" + str(n_iters), end="")
            self.update(dt=dt, character=character)
            x.append(self.pos[:, 0:1].T)            # shape --> [(1, n_particles), ...]
            y.append(self.pos[:, 1:2].T)            # shape --> [(1, n_particles), ...]
        x = np.vstack(x)                            # shape --> (n_iters, n_particles)
        y = np.vstack(y)                            # shape --> (n_iters, n_particles)
        traj = np.stack([x, y], axis=-1)            # shape --> (n_iters, n_particles, 2)
        return traj[::return_every_nth_step]

    def animated_example(self, dt: float, character: str, window_size: int = 1, marker_size: int = 5, update_interval=10):
        """Runs an exampe and animates the result"""

        # Scatterplot
        fig = plt.figure()
        ax = plt.scatter(x=system.pos[:, 0], y=system.pos[:, 1], cmap="seismic", s=marker_size)
        plt.xlim(self.min_coord * window_size, self.max_coord * window_size)
        plt.ylim(self.min_coord * window_size, self.max_coord * window_size)

        def animate(_):  # We don't care about the input parameter
            self.update(dt=dt, character=character)
            x=system.pos[:, 0]
            y=system.pos[:, 1]
            ax.set_offsets(np.c_[x,y])

        ani = FuncAnimation(fig, animate, interval=update_interval)
        plt.show()

    def plotted_example(self, dt: float, n_iters: int, character: str, window_size: int = 1, marker_size: int = 2, cmap='twilight', plot_every_n: int = 10, figsize=(7, 7)):
        x = np.expand_dims(self.pos[:, 0], axis=0)
        y = np.expand_dims(self.pos[:, 1], axis=0)
        X, Y = [x], [y]
        for i in range(n_iters-1):
            if i % 1000 == 0:
                print("\rIteration " + str(i) + "/" + str(n_iters), end="")
            self.update(dt=dt, character=character)
            X.append(self.pos[:, 0])
            Y.append(self.pos[:, 1])
        X = np.vstack(X)
        Y = np.vstack(Y)

        # Plotting setup
        X_plot = X[::plot_every_n]
        Y_plot = Y[::plot_every_n]
        nsteps = len(X_plot)
        colors = np.expand_dims(np.linspace(0, nsteps, nsteps), axis=-1)
        colors = np.repeat(colors, repeats=x.shape[-1], axis=-1)
        # Plot
        fig, ax = plt.subplots(figsize=figsize)
        plot = ax.scatter(X_plot, Y_plot, s=marker_size, c=colors, cmap=cmap)
        plt.colorbar(plot, label='time $\\times 1/'+str(plot_every_n)+'$', shrink=0.5)
        ax.set_xlim(self.min_coord * window_size, self.max_coord * window_size)
        ax.set_ylim(self.min_coord * window_size, self.max_coord * window_size)
        ax.set_aspect(1)
        plt.show()


def save_npy(array, basepath = None, filename = None):
    """Saves a numpy array to a .npy file. 
    
    Args: 
        array -- numpy array
        basepath -- directory where the file is saved to. Default is `Path().absolute().joinpath("generated_data/monopoles_gravity")`
        filenamee -- the filename to use. Default is `"simulation_" + datetime + "_" + milliseconds + ".npy"`
    Returns:
        None
    """
    if basepath is None:
        basepath = Path().absolute().joinpath("generated_data/monopoles_gravity")
    if filename is None:
        date_time = datetime.now().strftime("%Y%m%d-%H%M")
        milliseconds = str(round(time.time() * 1000))
        filename = "simulation_" + date_time + "_" + milliseconds + ".npy"
    filepath = basepath.joinpath(filename)
    os.makedirs(basepath, exist_ok=True)
    print("\nSaving .npy to", filepath)
    np.save(str(filepath), array)
    return


def generate_data(
    n_simulations: int = 1,
    n_particles: int = 2,
    max_coord: float = 0.5,
    min_coord: float = -0.5,
    max_vel: float = 1,
    min_vel: float = -1,
    min_mass: int = 1,
    max_mass: int = 3,
    min_charge: int = -1, 
    max_charge: int =1, 
    exclude_zero_charge: bool = True,
    G: float = 1,
    character: str = 'both',
    n_onehot_mass_values: int = 3,
    n_onehot_charge_values: int = 2,
    force_multiplier: float = 1.0,
    limit_range_of_force = False,
    range_of_force = np.inf,
    dt: float = 0.00001,
    n_iters: int = 1000,
    return_every_nth_step: int = 1,
    save_to_file=False,
    
    ):
    """"Runs the simulation multiple times with random initial values, saving the
    result to a file (if save_to_file==True).
    
    Args:
        queue -- a multiprocessing.Queue object for saving the return values to.
    Returns:
        array of shape (n_simulations, n_iters//return_every_nth_step, n_patricles, 2 + n_onehot_mass_values)
    """

    assert type(min_mass) == int and type(max_mass) == int, "min_mass and max_mass must be given as integers"
    assert n_onehot_mass_values >= max_mass - min_mass + 1, "The number of distinct mass values the one-hot encoding can express (n_onehot_mass_values) must be greater than or equal to the number of possible mass values (max_mass - min_mass + 1)."

    data = []
    for i in range(n_simulations):
        print("\rRunning simulation " + str(i+1) + "/" + str(n_simulations))
        # Inititalise the many body system
        system = System2D(
            n_particles=n_particles, 
            min_coord=min_coord,
            max_coord=max_coord,
            min_vel=min_vel,
            max_vel=max_vel,
            min_mass=min_mass,
            max_mass=max_mass,
            min_charge=min_charge, 
            max_charge=max_charge, 
            exclude_zero_charge=exclude_zero_charge,
            G=G,
            force_multiplier=force_multiplier,
            limit_range_of_force=limit_range_of_force,
            range_of_force=range_of_force,
        )
        # Run a simulation
        traj = system.run(dt=dt, n_iters=n_iters, character=character, return_every_nth_step=return_every_nth_step)  # shape --> (None, n_particles, 2)
        # Identity martix whose rows will be used as onehot vectors 
        eye = np.eye(n_onehot_mass_values)                                                      # shape --> (n_onehot_mass_values, n_onehot_mass_values)
        # From the identity matrix, select rows based on the particle masses. 
        # Subtract 1 from the mass because indexing starts from 0. 
        onehot = eye[(system.m - 1).astype(int)]  # -1 because smallest mass is 1               # shape --> (nparticles, n_onehot_mass_values)
        onehot = np.expand_dims(onehot, axis=0)                                                 # shape --> (1, nparticles, n_onehot_mass_values)
        onehot = np.repeat(onehot, repeats=traj.shape[0], axis=0)                               # shape --> (None, nparticles, n_onehot_mass_values)
        traj = np.concatenate([traj, onehot], axis=-1)                                # shape --> (None, nparticles, 2 + n_onehot_mass_values)
        if character == 'both' or character == 'coulomb':
            # Make onehot vector for charge
            eye = np.eye(n_onehot_charge_values)
            # remove zero charge
            print("\n\n==> DANGER!")
            print("    Removing zero charge. If you have more than two opposite")
            print("    charges (i.e. more than just {+1, -1} or {+2, -2}, etc. )")
            print("    then the zero charge should not be removed because you ")
            print("    want the magnitudes of the charges to be comparable!")
            print()
            print("    To change this behaviour, just edit the source code at the")
            print("    location of this print statement.\n")
            # Remove zero charge
            q = system.q
            q[system.q > 0] -= min(system.q[system.q > 0])
            # Shift values to the range [0, something]
            q = (q - min(q)).astype(int)
            onehot_c = eye[q]  # subtract because q may be negative
            onehot_c = np.expand_dims(onehot_c, axis=0)
            onehot_c = np.repeat(onehot_c, repeats=traj.shape[0], axis=0)
            traj = np.concatenate([traj, onehot_c], axis=-1)
        # Concatenate onehot encoding to each particle, after the position coordinates
        traj = np.expand_dims(traj, axis=0)                                                     # shape --> (1, None, nparticles, 2 + n_onehot_mass_values)
        data.append(traj)
    data = np.vstack(data)                                                                      # shape --> (n_simulations, None, nparticles, 2 + n_onehot_mass_values)   

    if save_to_file:
        save_npy(data)
    
    return data
    

def generate_data_files(
    n_files,
    n_simulations,
    n_particles,
    min_mass,
    max_mass,
    min_charge,
    max_charge,
    exclude_zero_charge,
    n_onehot_mass_values,
    n_onehot_charge_values,
    min_coord,
    max_coord,
    min_vel,
    max_vel,
    force_multiplier,
    limit_range_of_force,
    range_of_force,
    G,
    dt,
    n_iters,
    character,
    return_every_nth_step,
    n_parallell_processess=1,
    ):
    """Calls generate_data for n_files number of times, saving the result
    of each call in a separate npy file. The number of files (n_files) must 
    be divisible by the number of parallell processess (n_parallell_processess) 
    the work load is spread out over.

    Args:
        n_files -- number of files with simulation data to generate. Must be divisible by n_parallell_processess.
        n_parallell_processess -- the number of processess to run in parallell.
    Returns:
        None
    """
    assert n_files % n_parallell_processess == 0, "n_files must be divisible by n_parallell_processess"
    assert n_parallell_processess <= n_files, "n_parallell_processes must be less than or equal to n_files. Each file is created on a single core. Generating multiple files can be spread over multiple processess (cores)."

    for i in range(n_files // n_parallell_processess):
        print("Generating simulation file set ", i+1, "/", n_files//n_parallell_processess, sep="")
        # Define key word arguments
        kwargs = {
            'n_simulations': n_simulations, 
            'n_particles': n_particles, 
            'min_mass': min_mass,
            'max_mass': max_mass,
            'n_onehot_mass_values': n_onehot_mass_values,
            'n_onehot_charge_values': n_onehot_charge_values,
            'min_coord': min_coord,
            'max_coord': max_coord,
            'min_vel': min_vel,
            'max_vel': max_vel,
            'force_multiplier': force_multiplier,
            'limit_range_of_force': limit_range_of_force,
            'range_of_force': range_of_force,
            'dt': dt,
            'n_iters': n_iters,
            'return_every_nth_step': return_every_nth_step,
            'save_to_file': True,
            'min_charge': min_charge, 
            'max_charge': max_charge, 
            'exclude_zero_charge': exclude_zero_charge,
            'G': G,
            'character': character,
        }
        # Run multiple processess in parallell
        processes = {}
        for p in range(n_parallell_processess):
            processes[p] = Process(target=generate_data, kwargs=kwargs)
            processes[p].start()
        for key in processes:
            processes[key].join()
    return        


if __name__ == "__main__":
    system = System2D(
        n_particles=20, 
        min_mass=1, max_mass=3, 
        min_charge=-1, max_charge=1, exclude_zero_charge=True,
        min_coord=-0.5, max_coord=0.5, 
        min_vel=-1, max_vel=1, 
        force_multiplier=1, G=0.1,
        limit_range_of_force=False, range_of_force=0.2
    )
    # TODO: Uncomment this for an animated example
    #system.animated_example(dt=0.001, character='both', window_size=2, marker_size=1,update_interval=1)

    # TODO: Uncomment this for a plotted example
    #system.plotted_example(dt=0.0000001, n_iters=100000, character='both', plot_every_n=1000, window_size=2, marker_size=1)
    
    kwargs = {
        'n_files': 1,
        'n_parallell_processess': 1,
        'n_simulations': 1,
        'n_particles': 20, 
        'min_mass': 1,
        'max_mass': 3,
        'min_charge': -1, 
        'max_charge': 1, 
        'exclude_zero_charge': True,
        'n_onehot_mass_values': 3,
        'n_onehot_charge_values': 2,
        'min_coord': -0.5,
        'max_coord': 0.5,
        'min_vel': -1,
        'max_vel': 1,
        'force_multiplier': 1,
        'G': 0.1,
        'limit_range_of_force': False,
        'range_of_force': 0.2,
        'dt': 0.0000001,  # TODO: adjust precision with this. Smaller integration steps give more accurate results but require more iterations to simulate the same real time.
        'n_iters': 1000000,  # TODO: adjust the number of iterations with this. More iterations give longer simulations.
        'character': 'both',  # Options are 'gravity', 'coulomb', 'both'
        'return_every_nth_step': 1000,
    }
    generate_data_files(**kwargs)
    
    
        
    

