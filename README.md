# Simple Molecular Dynamics

Simple molecular dynamics. Numerically integrate Newton's equations of motion in two dimensions using velocity Verlet integration. 

# How to run it

```
python velocity-verlet-integrator.py
```

You can choose between three different particle interactions: `gravity`, `coulomb` or `both`. Think of these as `purely attractive`, `attractive or repulsive (depending on particle properties)`, `use both types of forces`.

> Note: Even though there are constants like the gravitatonal constant $`G`$ specified in the code, these are absolutely arbitrary.

Take a look at the `if __name__ == "__main__":` section of the python file. There you can specify the size of the time steps, simulation length, particle properties, etc. There is also an animated an a plotted example of the molecular dynamics simulator.

<img alt="The computational graph of the model" src="images/trajectories.png" width=70%>
